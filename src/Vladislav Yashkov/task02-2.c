#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define MAX_LENGTH 100
void Swap_rows(char** arr, int i, int j) {
	char* tmp = arr[i];
	arr[i] = arr[j];
	arr[j] = tmp;
}
void PrintRow(char* row, int br) {
	int i = 0;
	while (row[i] != '\0' && row[i] != '\n') {
		printf("%c", row[i]);
		i++;
	}
	if(br)
		printf("\n");
}
void PrintWords(char** words, int num) {
	for (int i = 0; i < num; i++) {
		PrintRow(words[i], false);
		printf(" ");
	}
	printf("\n");
}
void RandomRow(char** row, int num) {
	for (int i = 0; i < num; i++) {
		int random = rand() % (num - i) + i;
		Swap_rows(row, i, random);
	}
}
void RearrangeRow(char* row){
	char **words;
	int num = 1;
	int i = 0;
	int word_len = 0;
	words = (char**)calloc(num, sizeof(char*));
	while (row[i] != '\0' && row[i] != '\n')
		i++;
	row[i] = ' ';
	row[i + 1] = '\n';
	i = 0;
	while (row[i] != '\n') {
		if (row[i] == ' ') {
			if (word_len > 0) {
				word_len++;
				words[num-1] = (char*)realloc(words[num-1], word_len * sizeof(char));
				words[num-1][word_len - 1] = '\n';
				word_len = 0;
				num++;
				words = (char**)realloc(words, num * sizeof(char*));
			}
		}
		else {
			word_len++;
			if(word_len==1)
				words[num-1] = (char*)calloc(word_len, sizeof(char));
			else
				words[num-1] = (char*)realloc(words[num-1], word_len* sizeof(char));
			words[num-1][word_len - 1] = row[i];
		}
		i++;
	}
	RandomRow(words, num-1);
	PrintWords(words,num-1);
}
int main(){
	srand(time(0));
	setlocale(LC_ALL, "Russian");
	FILE* fp = fopen("file.txt", "r");
	if (fp == NULL) {
		printf("Error path!\n");
		return 1;
	}
	char row[MAX_LENGTH+1];
	while (fgets(row, MAX_LENGTH, fp)) {
		RearrangeRow(row);
	}
	return 0;
}
